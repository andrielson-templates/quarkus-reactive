package com.gitlab.andrielson.quarkus_reactive.users;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

@ApplicationScoped
public class UserService {

    private final PasswordEncoder passwordEncoder;

    @Inject
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Uni<User> create(@NotNull CreateUserDto createUserDto) {
        var user = new User(createUserDto.name(), createUserDto.email(), this.passwordEncoder.encode(createUserDto.password()));
        return user.persistAndFlush();
    }

    public Multi<User> findAll() {
        return User.streamAll();
    }
}
