package com.gitlab.andrielson.quarkus_reactive.users;

import io.quarkus.vertx.web.Body;
import io.quarkus.vertx.web.ReactiveRoutes;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;

@ApplicationScoped
public class UserRoutes {
    private static final String PATH = "/users";
    private final UserService userService;

    @Inject
    public UserRoutes(UserService userService) {
        this.userService = userService;
    }

    @Route(
            path = PATH,
            methods = Route.HttpMethod.POST,
            consumes = ReactiveRoutes.APPLICATION_JSON,
            produces = ReactiveRoutes.APPLICATION_JSON)
    public Uni<UserResource> create(@Body @Valid CreateUserRequest createUserRequest) {
        return userService.create(createUserRequest.mapToDto()).map(UserResource::fromUser);
    }

    @Route(
            path = PATH,
            methods = Route.HttpMethod.GET,
            produces = ReactiveRoutes.APPLICATION_JSON)
    public Multi<UserResource> list() {
        return userService.findAll().map(UserResource::fromUser);
    }
}
