package com.gitlab.andrielson.quarkus_reactive.users;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "users")
public class User extends PanacheEntity {
    public String name;
    public String email;
    public String password;
    public String role;
    @CreationTimestamp
    public Instant createdAt;
    @UpdateTimestamp
    public Instant updatedAt;

    public User() {
    }

    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = "user";
    }
}
