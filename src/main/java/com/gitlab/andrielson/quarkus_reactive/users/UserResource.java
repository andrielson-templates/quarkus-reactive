package com.gitlab.andrielson.quarkus_reactive.users;

import java.time.Instant;

public record UserResource(String name, String email, String role, Instant createdAt, Instant updatedAt) {
    public static UserResource fromUser(User user) {
        return new UserResource(user.name, user.email, user.role, user.createdAt, user.updatedAt);
    }
}
