package com.gitlab.andrielson.quarkus_reactive.users;

public record CreateUserDto(String name, String email, String password, String role) {
}
