package com.gitlab.andrielson.quarkus_reactive.users;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public record CreateUserRequest(
        @NotBlank
        String name,
        @Email
        String email,
        @NotBlank
        String password,
        @NotBlank
        String role) {

        public CreateUserDto mapToDto() {
                return new CreateUserDto(name, email, password, role);
        }
}
