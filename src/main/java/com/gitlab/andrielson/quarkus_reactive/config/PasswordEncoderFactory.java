package com.gitlab.andrielson.quarkus_reactive.config;

import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

public class PasswordEncoderFactory {

    @Produces @ApplicationScoped
    public PasswordEncoder passwordEncoder() {
        return new Argon2PasswordEncoder();
    }
}
